
LICENSE = "CLOSED"

SRC_URI = "git://isrc.iscas.ac.cn/gitlab/riscv/raios/u-boot-nezha.git;protocol=https;branch=main \
          "

SRCREV = "29fa9a13e4d4b4354d05c78b55d3f3ff8dff6c45"
S = "${WORKDIR}/git"

SYSROOT_DIRS += "/image"
PROVIDES += "u-boot"

do_configure() {
   make  sun20iw1p1_defconfig
}

do_compile() {
   make V=1 -j16
} 

DEPENDS:append = " across-toolchain-nezha-native"
DEPENDS:append = " u-boot-tools-native"

do_install () {
   install -d ${D}/image
	install -m 0755  ${S}/u-boot-sun20iw1p1.bin ${D}/image/u-boot-sun20iw1p1.fex
}

FILES:${PN} += " /image/ \
               "