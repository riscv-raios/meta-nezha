
LICENSE = "Unknown"
LIC_FILES_CHKSUM = "file://common/lzma/license.txt;md5=8ecc4f7ef807bbf661bf65387dc7cd08"

SRC_URI = "git://isrc.iscas.ac.cn/gitlab/riscv/raios/spl-nezha.git;protocol=https;branch=devtool \
           file://0002-update-toolchain-path.patch \
           "

PV = "1.0-git"
SRCREV = "d4861f226f22e2d55582eaff471976b58eb83021"

S = "${WORKDIR}/git"

# NOTE: this is a Makefile-only piece of software, so we cannot generate much of the
# recipe automatically - you will need to examine the Makefile yourself and ensure
# that the appropriate arguments are passed in.

do_configure () {
     make p=sun20iw1p1 b=d1-h
}

do_compile () {
	export LDFLAGS=" "
     make -j b=d1-h CPU=riscv64
}

do_install () {
     install -d ${D}/image
     install -m 0755  ${S}/fes/fes1_sun20iw1p1.bin ${D}/image/fes1.fex
     install -m 0755  ${S}/nboot/boot0_*_*.bin ${D}/image/
}

FILES:${PN} += " \
    image/ \
"

SYSROOT_DIRS += "/image"
DEPENDS:append = " across-toolchain-nezha-native"