
LICENSE = "Unknown"
LIC_FILES_CHKSUM = "file://COPYING.BSD;md5=42dd9555eb177f35150cf9aa240b61e5"

SRC_URI = "git://isrc.iscas.ac.cn/gitlab/riscv/raios/opensbi-nezha.git;protocol=https;branch=main \
           file://0001-absolute-cross-compile-update.patch \
           "

PV = "1.0+git${SRCPV}"
SRCREV = "6f87a2926564355e107b6b43534c2a2979c5d696"

S = "${WORKDIR}/git"

do_configure () {
	:
}

do_compile () {
	make PLATFORM=thead/c910 CROSS_COMPILE=riscv64-unknown-linux-gnu- \
		 SUNXI_CHIP=sun20iw1p1 PLATFORM_RISCV_ISA=rv64gcxthead
}

do_install () {
	# This is a guess; additional arguments may be required
	oe_runmake install
}

DEPENDS:append = " across-toolchain-nezha-native"

do_install () {
    install -d ${D}/image
	install -m 0755  ${S}/build/platform/thead/c910/firmware/fw_jump.bin \
					${D}/image/opensbi_sun20iw1p1.bin
}

FILES:${PN} += " \
    image/ \
"

SYSROOT_DIRS += "/image"