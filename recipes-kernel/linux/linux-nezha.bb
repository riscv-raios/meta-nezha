
require recipes-kernel/linux/linux-mainline-common.inc

LICENSE = "CLOSED"


SRC_URI = "git://isrc.iscas.ac.cn/gitlab/riscv/raios/linux-nezha.git;protocol=https;branch=main \
        "

SRC_URI:append = " file://defconfig"
SRC_URI:append = " file://board.dts"

SRCREV = "9352a0a961545d6fdface3b3df9a6aa1c1d46830"

COMPATIBLE_MACHINE = "nezha"

KERNEL_FEATURES:remove = "cfg/fs/vfat.scc"
KERNEL_VERSION_SANITY_SKIP="1"

PV = "1.0+git${SRCPV}"
S = "${WORKDIR}/git"

DEPENDS:append = " across-toolchain-nezha-native"

do_patch:append() {
    install -m 0755 ${WORKDIR}/board.dts \
                    ${S}/arch/riscv/boot/dts/sunxi/board.dts    
}

