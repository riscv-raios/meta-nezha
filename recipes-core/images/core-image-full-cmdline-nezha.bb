DESCRIPTION = "A console-only image with more full-featured Linux system \
functionality installed."

IMAGE_INSTALL = "\
		initramfs-framework-base \
        busybox \
        base-passwd \
    "

inherit core-image


do_copy_file () {
	rm ${IMAGE_ROOTFS}/init
    install -m 0755 ${IMAGE_ROOTFS}/bin/busybox \
        ${IMAGE_ROOTFS}/init

}

addtask do_copy_file after do_rootfs before do_image
