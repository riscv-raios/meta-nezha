SUMMARY = "GNU Project parser generator (yacc replacement)"
DESCRIPTION = "Bison is a general-purpose parser generator that converts an annotated context-free grammar into \
an LALR(1) or GLR parser for that grammar.  Bison is upward compatible with Yacc: all properly-written Yacc \
grammars ought to work with Bison with no change. Anyone familiar with Yacc should be able to use Bison with \
little trouble."
HOMEPAGE = "http://www.gnu.org/software/bison/"
LICENSE = "CLOSED"

SRC_URI = "git://isrc.iscas.ac.cn/gitlab/riscv/raios/pack-bintools-nezha.git;protocol=https;branch=main \
          "
SRCREV = "2f0f41f4f0172701741cc0e749bc60c528dbc303"
S = "${WORKDIR}/git"

inherit gettext texinfo


do_compile:prepend() {
	:
}

do_install() {
    install -d ${D}${bindir}/pack-bintools
    install -m 0755 ${S}/* ${D}${bindir}/pack-bintools
}
INSANE_SKIP:${PN} += "already-stripped"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"

INHIBIT_SYSROOT_STRIP = "1"
FILES:${PN} += "${bindir}/pack-bintools/*"

deltask do_package_qa

do_install:append:class-native() {
    :
}

BBCLASSEXTEND = " native"