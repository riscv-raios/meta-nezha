SUMMARY = "GNU Project parser generator (yacc replacement)"
DESCRIPTION = "Bison is a general-purpose parser generator that converts an annotated context-free grammar into \
an LALR(1) or GLR parser for that grammar.  Bison is upward compatible with Yacc: all properly-written Yacc \
grammars ought to work with Bison with no change. Anyone familiar with Yacc should be able to use Bison with \
little trouble."
HOMEPAGE = "http://www.gnu.org/software/bison/"
LICENSE = "CLOSED"

SRC_URI = "file://riscv64-glibc-gcc-thead_20200702.tar.xz \
           "

inherit gettext texinfo

do_compile:prepend() {
    :
}

do_install() {
    install -d ${D}${bindir}/nezha-toolchain
    cp -r ${WORKDIR}/riscv64-glibc-gcc-thead_20200702/* \
            ${D}${bindir}/nezha-toolchain
}
INSANE_SKIP:${PN} += "already-stripped"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_SYSROOT_STRIP = "1"

FILES:${PN} += "${bindir}/nezha-toolchain/*"
deltask do_package_qa

BBCLASSEXTEND = " native"