SRC_URI = "file://Image.gz-no-ext \
            file://boot-package \
            file://do-common \
            file://do-finish \
            file://do-pack \
            file://sdk-files \
           "

inherit deploy 

S = "${WORKDIR}/pack-image"

DEPENDS:append = " linux-nezha"
DEPENDS:append = " u-boot-nezha"
DEPENDS:append = " spl-nezha"
DEPENDS:append = " opensbi-nezha"
DEPENDS:append = " core-image-full-cmdline-nezha"

do_compile[depends] += "core-image-full-cmdline-nezha:do_image_complete"
PATH:prepend = "${STAGING_DIR_NATIVE}/usr/bin/pack-bintools:"


do_deploy() {
    install -m 0755 ${S}/tina_d1-h-nezha_uart0.img \
                    ${DEPLOYDIR}/tina_d1-h-nezha_uart0.img
}

INSANE_SKIP:${PN} += "already-stripped"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"

deltask do_package_qa

SYSROOT_DIRS_NATIVE += " pack-bintools-dev"
SYSROOT_DIRS += " pack-bintools-dev"

DEPENDS:append = " apack-bintools-native"

DEPENDS:append = " python3-native"
LICENSE = "CLOSED"

DEPENDS:append = " across-toolchain-nezha-native"

do_compile() {
    install -d ${S}

	if [ -f ${WORKDIR}/recipe-sysroot-native/usr/bin/python ]; then
        	rm -f ${WORKDIR}/recipe-sysroot-native/usr/bin/python
	fi
    ln -s ${WORKDIR}/recipe-sysroot-native/usr/bin/python3-native/python3.10 \
             ${WORKDIR}/recipe-sysroot-native/usr/bin/python

    cp -f ${WORKDIR}/do-common/script ${WORKDIR}/recipe-sysroot-native/usr/bin/script

    cp  ${STAGING_DIR_HOST}/image/opensbi_sun20iw1p1.bin ${S}/opensbi.fex
    cp ${WORKDIR}/do-common/sys_config.fex ${S}/sys_config.fex
    busybox unix2dos ${S}/sys_config.fex
	script  ${S}/sys_config.fex

## 1. do prepare
    cp ${STAGING_DIR_HOST}/image/fes1.fex ${S}/fes1.fex
    update_fes1  ${S}/fes1.fex  ${S}/sys_config.bin

    cp ${STAGING_DIR_HOST}/image/boot0_nand_sun20iw1p1.bin ${S}/boot0_nand.fex
    cp ${STAGING_DIR_HOST}/image/boot0_sdcard_sun20iw1p1.bin ${S}/boot0_sdcard.fex

    update_boot0 ${S}/boot0_nand.fex     ${S}/sys_config.bin NAND
    update_boot0 ${S}/boot0_sdcard.fex   ${S}/sys_config.bin SDMMC_CARD

    # update_chip
    update_chip boot0_nand.fex || echo "$?"
    update_chip boot0_sdcard.fex || echo "$?"


    cp ${STAGING_DIR_HOST}/image/u-boot-sun20iw1p1.fex ${S}/u-boot.fex
    update_uboot -no_merge u-boot.fex sys_config.bin 

	update_fes1  fes1.fex           sys_config.bin 
	# fsbuild	     boot-resource.ini  split_xxxx.fex 



## 2.do_ini_to_dts
    cp ${DEPLOY_DIR_IMAGE}/board.dtb ${S}/sunxi.dtb
    cp ${S}/sunxi.dtb ${S}/sunxi.fex
	update_dtb ${S}/sunxi.fex 4096

### 3. do common
    cp ${WORKDIR}/do-common/sys_config.fex ${S}/sys_config.fex
    busybox unix2dos ${S}/sys_config.fex
	script  ${S}/sys_config.fex 
    cp -f   ${S}/sys_config.bin ${S}/config.fex

    # cp ${WORKDIR}/do-common/board_config.fex ${S}/board_config.fex
	# busybox unix2dos ${S}/board_config.fex
	# script  ${S}/board_config.fex 
	cp -f ${WORKDIR}/do-common/board_config.bin ${S}/board.fex


    cp ${WORKDIR}/do-common/sys_partition.fex ${S}/sys_partition.fex
    busybox unix2dos ${S}/sys_partition.fex
	script  ${S}/sys_partition.fex 

    update_dtb  ${S}/sunxi.fex 4096

    update_boot0 ${S}/boot0_nand.fex	${S}/sys_config.bin NAND 
    update_boot0 ${S}/boot0_sdcard.fex	${S}/sys_config.bin SDMMC_CARD 

    # update_uboot -no_merge  ${S}/u-boot.fex  ${S}/sys_config.bin 

    update_fes1   fes1.fex            sys_config.bin 


	cp -f ${WORKDIR}/do-common/boot-resource.ini ${S}/boot-resource.ini
	cp -f ${WORKDIR}/do-common/split_xxxx.fex ${S}/split_xxxx.fex
	# fsbuild	     boot-resource.ini   split_xxxx.fex 


	cp -f ${WORKDIR}/do-common/boot_package.cfg ${S}/boot_package.cfg
    busybox unix2dos ${S}/boot_package.cfg
    dragonsecboot -pack ${S}/boot_package.cfg

	# cp -f ${WORKDIR}/do-common/env.cfg ${S}/env.cfg
    # mkenv ${S}/env.cfg ${S}/env.fex
    cp -f ${WORKDIR}/do-common/env.fex ${S}/env.fex

## 4. do_pack_tina
    cp ${DEPLOY_DIR_IMAGE}/Image.gz ${S}/Image.gz
    mkbootimg \
        --kernel  ${S}/Image.gz \
        --board  d1-h-nezha --base  0x40000000 \
        --kernel_offset  0x0 \
        -o  ${S}/d1-h-nezha-boot.img

    cp -fpR ${S}/d1-h-nezha-boot.img ${S}/boot.img
    ln -s ${S}/boot.img        ${S}/boot.fex
    ln -s ${S}/boot.fex        ${S}/kernel.fex

        # rm rootfs.fex
    cp -f ${WORKDIR}/do-finish/boot-resource.fex  ${S}/boot-resource.fex 
    cp -f ${DEPLOY_DIR_IMAGE}/core-image-full-cmdline-nezha-nezha.squashfs-xz  \
            ${S}/root.squashfs 
    dd if=${S}/root.squashfs of=${S}/rootfs.img bs=128k conv=sync
    ln -s ${S}/rootfs.img     ${S}/rootfs.fex
    
    # ln -s ${ROOT_DIR}/rootfs.img     rootfs.fex

    ln -s ${S}/rootfs.fex ${S}/rootfs_nor.fex
	ln -s ${S}/rootfs.fex ${S}/rootfs-ubifs.fex
    
    cp -f ${WORKDIR}/do-pack/recovery.fex  ${S}/recovery.fex 
    # cp ${XXX}/recovery.fex ${S}/recovery.fex


 ## 5. do finish

    cp -f ${WORKDIR}/do-finish/dlinfo.fex  ${S}/dlinfo.fex 

    awk 'BEGIN { cmd="cp -ri ${WORKDIR}/sdk-files/*  ${S}/"; print "n" |cmd; }'
    # cp -n ${WORKDIR}/sdk-files/*  ${S}/

    update_mbr ${S}/sys_partition.bin 4

    update_mbr ${S}/sys_partition.bin 4 ${S}/sunxi_mbr.fex ${S}/dlinfo.fex 15269888 20 0
    cp ${S}/sys_partition.fex ${S}/sys_partition_for_dragon.fex
    cp -f ${WORKDIR}/do-finish/image.cfg  ${S}/image.cfg 

    # do_dragon ${S}/image.cfg ${S}/sys_partition_for_dragon.fex

## 6. do dragon
    
    cp -f ${WORKDIR}/do-finish/dragon  ${STAGING_DIR_NATIVE}/usr/bin/pack-bintools/dragon 
    cp -f ${WORKDIR}/do-finish/dragon  ${S}/dragon 
    ${S}/dragon image.cfg sys_partition_for_dragon.fex

}


addtask deploy after do_install